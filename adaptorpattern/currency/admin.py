from django.contrib import admin
from currency.models import Currencies, Provider


class CurrunciesAdmin(admin.ModelAdmin):
    search_fields = ('usd', )
    list_display = ['provider_name', 'usd', 'eur', 'gbp', ]


class ProviderAdmin(admin.ModelAdmin):
    list_display = ['provider_name', 'provider_url']


admin.site.register(Currencies, CurrunciesAdmin)
admin.site.register(Provider, ProviderAdmin)
