from django.shortcuts import render
from currency.adapter import tales, blackbull
from currency.models import Currencies
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.db.models import Min
from currency.serializers import CurrenciesSerializer


class ProviderView(APIView):
    def get(self, request, format=None):
        response_blackbull = blackbull.Adapter().get_request()
        response_tales = tales.Adapter().get_request()
        return Response((response_blackbull, response_tales),
                        status=status.HTTP_200_OK)


class Cheapest(APIView):
    def get(self, request, format=None):
        response = Currencies.objects.aggregate(usd=Min('usd'), eur=Min('eur'),
                                                gbp=Min('gbp'))
        serializer = CurrenciesSerializer(response)
        return Response(serializer.data)
