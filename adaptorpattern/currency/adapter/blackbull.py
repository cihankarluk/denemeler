from currency.clients import blackbull
from currency.models import Currencies


class Adapter(object):
    def get_request(self):
        response = blackbull.Client().make_request()
        response = response.get('result')
        Currencies(
            provider_name='blackbull',
            usd=response[0].get('amount'),
            eur=response[1].get('amount'),
            gbp=response[2].get('amount')
        ).save()
        return response
