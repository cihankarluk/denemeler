from currency.clients import tales
from currency.models import Currencies


class Adapter(object):
    def get_request(self):
        response = tales.Client().make_request()
        Currencies(
            provider_name='tales',
            usd=response[0].get('oran'),
            eur=response[1].get('oran'),
            gbp=response[2].get('oran')
        ).save()
        return response
