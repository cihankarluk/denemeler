# Generated by Django 2.1.5 on 2019-02-07 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('currency', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('provider_name', models.CharField(max_length=20)),
                ('provider_url', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'providers',
            },
        ),
    ]
