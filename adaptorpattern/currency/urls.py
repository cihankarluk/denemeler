from django.urls import path
from currency import views

urlpatterns = [
    path('init', views.ProviderView.as_view(), name='init'),
    path('', views.Cheapest.as_view(), name='karma')
]
