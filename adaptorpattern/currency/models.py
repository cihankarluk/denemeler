from django.db import models


class Currencies(models.Model):
    provider_name = models.CharField(max_length=20)
    usd = models.FloatField()
    eur = models.FloatField()
    gbp = models.FloatField()

    class Meta:
        db_table = 'currencies'

    def __str__(self):
        return self.provider_name + ' ' + str(self.usd) + ' ' + str(self.eur) \
               + ' ' + str(self.gbp)


class Provider(models.Model):
    provider_name = models.CharField(max_length=20)
    provider_url = models.CharField(max_length=50)

    class Meta:
        db_table = 'providers'

    def __str__(self):
        return self.provider_name + ' ' + self.provider_url
