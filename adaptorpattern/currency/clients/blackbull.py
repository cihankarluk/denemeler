import requests
from currency.models import Provider


class Client(object):
    def make_request(self):
        filtered_query_set = Provider.objects.filter(provider_name='blackbull')
        provider_object = filtered_query_set.first()
        url = provider_object.provider_url
        response = requests.get(url)
        req = response.json()
        return req
