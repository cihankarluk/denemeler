from rest_framework import serializers
from currency.models import Currencies


class CurrenciesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currencies
        fields = ('usd', 'eur', 'gbp')
